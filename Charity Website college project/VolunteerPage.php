<?php

if(isset($_POST['firstname']) && isset($_POST['lastname']) 
	&& isset($_POST['email']) && isset($_POST['tellNo']) 
	&& isset($_POST['whereBased']) && isset($_POST['where'])
	&& isset($_POST['experience']) ) 
	{
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];	
		$tellNo = $_POST['tellNo'];
		$whereBased = $_POST['whereBased'];
		$where = $_POST['where'];
		$experience = $_POST['experience'];
	}		
if(!empty($firstname) && !empty($lastname) 
	&& !empty($email) && !empty($tellNo) 
	&& !empty($whereBased) && !empty($where) 
	&& !empty($experience) ) 
	{
		$handle = fopen('volunteer.txt', 'a');	
		fwrite($handle, $firstname."\r\n");
		fwrite($handle, $lastname."\r\n");
		fwrite($handle, $email."\r\n");
		fwrite($handle, $tellNo."\r\n");
		fwrite($handle, $whereBased."\r\n");
		fwrite($handle, $where."\r\n");
		fwrite($handle, $experience."\r\n");	
		fclose($handle);	
	}
?>
<html>
<head lang="en">
<meta charset="utf-8">
<link rel="stylesheet" ,="" href="cssfile.css">
<script type="text/javascript" src="jscriptfile.js"></script>
</head>

<div id="pageHead">
<a href="Hompage.html"><img src="images\logo.jpg" id="logo"></a>
<div id="donate">
	<nav id="donateButton">
		<button><a href="http://localhost:5723/DonatePage.php">Donate</a></button> 
	</nav>
</div>
<div id="search">
	Search:&nbsp;<input type="search" name="search">
</div>

<div>
	<nav id="primary_nav_wrap">
		<ul>
			<li class="current-menu-item"><a href="Hompage.html">Home</a></li>	
			<li class="current-menu-item"><a href="AboutPage.html">About</a></li>	
			<li class="current-menu-item"><a href="WhereWeWorkPage.html">Where we work</a></li>	  
			<li class="current-menu-item"><a href="WhatWeDoPage.html">What we do</a>
				<ul>
					<li class="current-menu-item"><a href="ProtectionPage.html">Child Protection</a></li>
					<li class="current-menu-item"><a href="EducationPage.html">Child Education</a></li>
					<li class="current-menu-item"><a href="NutritionPage.html">Nutrition</a></li>
					<li class="current-menu-item"><a href="WaterPage.html">Water</a></li>
				</ul>
			</li>
			<li class="current-menu-item"><a href="Gallery.html">Gallery</a></li>	  
			<li class="current-menu-item"><a href="ContactPage.html">Contact</a></li>	
		</ul>
	</nav>
</div>
</div>
<div id="container">
<img id="pic1" src="Images\volunteerImage.jpg"/>
<h1 style="text-align:center;">Volunteer</h1>
<form action="VolunteerPage.php" method="POST" id="mainForm">
<center>
<p>
	<label>Frist Name: <br />
	<input type="text" name="firstname" class="required" />
</p>

<p>
	<label>Last Name: <br />
	<input type="text" name="lastname" class="required" />
</p>

<p>
	<label>Email Address: <br />
	<input type="email" name="email" class="required"/>
</p>

<p>
	<label>Telephone Number: <br />
	<input type="text" name="tellNo" class="required"/>
</p>

<p>
	<label>Where are you based? <br /><br />
	<select name="whereBased">
		<option>Co. Antrim</option>
		<option>Co. Armagh</option>
		<option>Co. Carlow</option>
		<option>Co. Cavan</option>
		<option>Co. Clare</option>
		<option>Co. Cork</option>
		<option>Co. Derry</option>
		<option>Co. Donegal</option>
		<option>Co. Down</option>
		<option>Co. Dublin</option>
		<option>Co. Fermanagh</option>
		<option>Co. Galway</option>
		<option>Co. Kerry</option>
		<option>Co. Kildare</option>
		<option>Co. Kilkenny</option>
		<option>Co. Laois</option>
		<option>Co. Leitrim</option>
		<option>Co. Limerick</option>
		<option>Co. Longford</option>
		<option>Co. Louth</option>
		<option>Co. Mayo</option>
		<option>Co. Meath</option>
		<option>Co. Monaghan</option>
		<option>Co. Offaly</option>
		<option>Co. Roscommon</option>
		<option>Co. Sligo</option>
		<option>Co. Tipperary</option>
		<option>Co. Tyrone</option>
		<option>Co. Waterford</option>
		<option>Co. Westmeath</option>
		<option>Co. Wexford</option>
		<option>Co. Wicklow</option>
	</select>
</p>

<p>
	<label>Where would you like to volunteer? <br /><br />
	<select name="where">
		<option>Where I'm based</option>
		<option>Where I'm needed</option>
	</select>
</p>

<p>
	<label>Do you have experience in any particular field? <br /><br />
	<select name="experience">
		<option>Agriculture</option>
		<option>Business</option>
		<option>Communication</option>
		<option>Construction</option>
		<option>Education</option>
		<option>Engineering</option>
		<option>Finance</option>
		<option>Health</option>
		<option>IT</option>
		<option>Livelihoods</option>
		<option>Logistics</option>
		<option>Management</option>
		<option>Security</option>
		<option>Water, Hygiene & Sanitation (WASH)</option>
		<option>Other</option>
	</select>
</p>

<input type="submit" value="Sign Me Up"><br /><br />
</form>
</center>
</body>
<footer>
<div id="footerWrap">
	<div id="Info">
		<h3>Information</h3>
		<ul>
			<a href="Hompage.html">Home</a><br/>
			<a href="AboutPage.html">About</a><br/>
			<a href="ContactPage.html">Contact</a><br/>
		</ul>
	</div>	
	<div id="AboutUs">
		<h3>About Us</h3>
		<ul>
			<a href="WhatWeDoPage.html">What We Do</a><br/>
			<a href="WhereWeWorkPage.html">Where We Work</a><br/>
			<a href="HowYouCanHelpPage.html">How You Can Help</a><br/>			
		</ul>
	</div>
	<div id="WhatsHappening">
		<h3>Whats Happening</h3>
		<ul>			
			<a href="http://localhost:5723/Events.php">Events</a><br/>
			<a href="TestimonialsPage.html">Testimonials</a><br/>
			<a href="Gallery.html">Gallery</a><br/>
		</ul>
	</div>		
	<div id="DonateAndSocial">
		<br/>
		<button><a href="http://localhost:5723/DonatePage.php">Donate</a></button><br/><br/><br/>  
		<a class="socialImg" href="https://www.facebook.com/"><img src="images\facebook.jpg" ></a>
		<a class="socialImg" href="https://twitter.com/"><img src="images\twitter.jpg" > </a> 
		<a class="socialImg" href="https://www.instagram.com/"><img src="images\instagram.jpg" > </a> 
		<a class="socialImg" href="https://www.youtube.com/"><img src="images\youtube.png"></a> 
	</div>			
</div>
</footer>
</html>