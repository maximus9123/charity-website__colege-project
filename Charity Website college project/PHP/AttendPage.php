<?php
if(isset($_POST['attend_firstname']) && isset($_POST['attend_lastname']) && isset($_POST['attend_email']) && isset($_POST['attend_number']) && isset($_POST['attend_eventname']) && isset($_POST['attend_eventdate'])) {
	$attend_firstname = $_POST['attend_firstname'];
	$attend_lastname = $_POST['attend_lastname'];
	$attend_email = $_POST['attend_email'];
	$attend_number = $_POST['attend_number'];
	$attend_eventname = $_POST['attend_eventname'];
	$attend_eventdate = $_POST['attend_eventdate'];
}

if(!empty($attend_firstname) && !empty($attend_lastname) && !empty($attend_email) && !empty($attend_number) && !empty($attend_eventname) && !empty($attend_eventdate)) {
	$handle = fopen('attend.txt', 'w');
	$name = 'Attend';
	fwrite($handle, $attend_firstname."\r\n");
	fwrite($handle, $attend_lastname."\r\n");
	fwrite($handle, $attend_email."\r\n");
	fwrite($handle, $attend_number."\r\n");
	fwrite($handle, $attend_eventname."\r\n");
	fwrite($handle, $attend_eventdate."\r\n");
	fclose($handle);
}
?>

<html>
<head>
<link rel="stylesheet", href="cssfile.css">
<img src="images\Icon.png" style="width:100px;height:100px;"/>

<h3 id="donate">
<input type="text" name="search" placeholder="Search.."></input>
<nav id="primary_nav_wrap">
<ul>
	<li class="current-menu-item"><a href="DonatePage.html">Donate</a></li>
</ul>
</h3>
</nav>
</head>

<body>
<div>
<nav id="primary_nav_wrap">
<ul>
  <li class="current-menu-item"><a href="HomePage.html">Home</a></li>
  <li class="current-menu-item"><a href="AboutPage.html">About</a></li>
  <li class="current-menu-item"><a href="WhereWeWorkPage.html">Where we work</a></li>
  <li class="current-menu-item"><a href="WhatWeDoPage.html">What we do</a></li>
  <li class="current-menu-item"><a href="ContactPage.html">Contact</a></li>
</ul>
</nav>
</div>

<center>
<br /><br /><br /><img id="image_slider" src="images\attendImage.jpg"/>
</center>

<br /><br /><h1 style="text-align:center;">Attend</h1>

<center>
<form action="AttendPage.php" method="POST">
	First Name: <br> <input type="text" name="attend_firstname"> <br><br>
	Last Name: <br> <input type="text" name="attend_lastname"> <br><br>
	Email Address: <br> <br> <input type="email" name="attend_email"> <br><br>
	Telephone Number: <br> <input type="text" name="attend_number"> <br><br>
	Event Name: <br> <input type="text" name="attend_eventname"> <br><br>
	Event Date: <br> <br> <input type="date" name="attend_eventdate"> <br><br>
	<input type="Submit" value="Submit">
</form>
</center>

<div style="background-color: #595959;">
<table>
  <tr>
    <th>Social Media</th>
    <th>Quick links</th> 
    <th>Latest news</th>
    <th>Donate</th>
  </tr>
  <tr>
    <td><img src="images\facebook.jpg" style="width:16px;height:16px;"/></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><img src="images\twitter.jpg" style="width:16px;height:16px;"/></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><img src="images\instagram.jpg" style="width:16px;height:16px;"/></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><img src="images\youtube.png" style="width:16px;height:16px;"/></td>
    <td></td>
    <td></td>
  </tr>
</table>
</div>
</body>
</html>