<html><head>
<link rel="stylesheet" href="cssfile.css">
<script type="text/javascript" src="jscriptfile.js"></script>
</head>
<body>
<?php
if(isset($_POST['donate_amount']) && isset($_POST['donate_appeal']) 
	&& isset($_POST['donate_firstname']) && isset($_POST['donate_lastname']) 
	&& isset($_POST['donate_email']) && isset($_POST['donate_number']) 
	&& isset($_POST['donate_address1']) && isset($_POST['donate_address2']) 
	&& isset($_POST['donate_citytown']) && isset($_POST['donate_county']) 
	&& isset($_POST['donate_cardname']) && isset($_POST['donate_cardnumber']) 
	&& isset($_POST['donate_expirydate']) && isset($_POST['donate_securitycode'])) {
	$donate_amount = 'Donation Amount:'.' '.$_POST['donate_amount'];
	$donate_appeal = 'Appeal:'.' '.$_POST['donate_appeal']; 
	$donate_firstname = 'First Name:'.' '.$_POST['donate_firstname'];
	$donate_lastname = 'Last Name:'.' '.$_POST['donate_lastname'];
	$donate_email = 'Email:'.' '.$_POST['donate_email'];
	$donate_number = 'DPhone No:'.' '.$_POST['donate_number'];
	$donate_address1 = 'Address:'.' '.$_POST['donate_address1'];
	$donate_address2 = 'Address:'.' '.$_POST['donate_address2'];
	$donate_citytown = 'City/Town:'.' '.$_POST['donate_citytown'];
	$donate_county = 'County:'.' '.$_POST['donate_county'];
	$donate_cardname = 'Cardholders Name:'.' '.$_POST['donate_cardname'];
	$donate_cardnumber = 'Card No:'.' '.$_POST['donate_cardnumber'];
	$donate_expirydate = 'Expiry Date:'.' '.$_POST['donate_expirydate'];
	$donate_securitycode = 'Security Code:'.' '.$_POST['donate_securitycode'];
	$donate_blank = ' ';
	
}

if(!empty($donate_amount) && !empty($donate_appeal) 
	&& !empty($donate_firstname) && !empty($donate_lastname) 
	&& !empty($donate_email) && !empty($donate_number) && !empty($donate_address1) 
	&& !empty($donate_address2) && !empty($donate_citytown) && !empty($donate_county) 
	&& !empty($donate_cardname) && !empty($donate_cardnumber) && !empty($donate_expirydate) 
	&& !empty($donate_securitycode)) {
	$handle = fopen('donate.txt', 'a');
	$name = 'Donate';
	fwrite($handle, $donate_amount."\r\n");
	fwrite($handle, $donate_appeal."\r\n");
	fwrite($handle, $donate_firstname."\r\n");
	fwrite($handle, $donate_lastname."\r\n");
	fwrite($handle, $donate_email."\r\n");
	fwrite($handle, $donate_number."\r\n");
	fwrite($handle, $donate_address1."\r\n");
	fwrite($handle, $donate_address2."\r\n");
	fwrite($handle, $donate_citytown."\r\n");
	fwrite($handle, $donate_county."\r\n");
	fwrite($handle, $donate_cardname."\r\n");
	fwrite($handle, $donate_cardnumber."\r\n");
	fwrite($handle, $donate_expirydate."\r\n");
	fwrite($handle, $donate_securitycode."\r\n");	
	fwrite($handle, $donate_blank."\r\n");
	fclose($handle);
}
?>



<div id="pageHead">
<a href="Hompage.html"><img src="images\logo.jpg" id="logo"></a>
<div id="donate">
	<nav id="donateButton">
		<button><a href="DonatePage.html">Donate</a></button> 
	</nav>
</div>
<div id="search">
	Search:&nbsp;<input type="text" name="search" placeholder="Search..">
</div>

<div>
	<nav id="primary_nav_wrap">
	<ul>
	  <li class="current-menu-item"><a href="Hompage.html">Home</a></li>	
	  <li class="current-menu-item"><a href="AboutPage.html">About</a></li>	
	  <li class="current-menu-item"><a href="WhereWeWorkPage.html">Where we work</a></li>	  
	  <li class="current-menu-item"><a href="WhatWeDoPage.html">What we do</a></li>		
	  <li class="current-menu-item"><a href="ContactPage.html">Contact</a></li>	
	</ul>
	</nav>
</div>
</div>
<div id="container">

<center>
<img id="pic1" src="Images\Donate.jpg"/>

<h1>Donate</h1>
</center>


<form action="DonatePage.php" method="POST">
<div id="donatePage">
<div class="donatePage">

<label>Amount: <br /><br />
<input type="radio" name="donate_amount" value="10">€10<br />
<input type="radio" name="donate_amount" value="20">€20<br />
<input type="radio" name="donate_amount" value="50">€50<br />
<input type="radio" name="donate_amount" value="100">€100<br />
<input type="radio" name="donate_amount" value="250">€250<br />
<input type="radio" name="donate_amount" value="500">€500<br />

</div>

<label>Appeal: <br /><br />
<select name="donate_appeal">
	<option value="Education">Child Education</option>
	<option value="Protection">Child Protection</option>
	<option value="Nutrition">Nutrition</option>
	<option value="Water">Water</option>		
	<option value="needed">Where it's needed most</option>
</select>

</p>

<p>
	<label>Frist Name: <br />
	<input type="text" name="donate_firstname" />
</p>

<p>
	<label>Last Name: <br />
	<input type="text" name="donate_lastname" />
</p>

<p>
	<label>Email: <br />
	<input type="email" name="donate_email" />
</p>

<p>
	<label>Telephone: <br />
	<input type="text" name="donate_number" />
</p>

<p>
	<label>Address 1: <br />
	<input type="text" name="donate_address1" />
</p>

<p>
	<label>Address 2: <br />
	<input type="text" name="donate_address2" />
</p>

<p>
	<label>City/Town: <br />
	<input type="text" name="donate_citytown" />
</p>

<p>
	<label>County: <br /><br />
	<select name="donate_county">
		<option>Antrim</option>
		<option>Armagh</option>
		<option>Carlow</option>
		<option>Cavan</option>
		<option>Clare</option>
		<option>Cork</option>
		<option>Derry</option>
		<option>Donegal</option>
		<option>Down</option>
		<option>Dublin</option>
		<option>Fermanagh</option>
		<option>Galway</option>
		<option>Kerry</option>
		<option>Kildare</option>
		<option>Kilkenny</option>
		<option>Laois</option>
		<option>Leitrim</option>
		<option>Limerick</option>
		<option>Longford</option>
		<option>Louth</option>
		<option>Mayo</option>
		<option>Meath</option>
		<option>Monaghan</option>
		<option>Offaly</option>
		<option>Roscommon</option>
		<option>Sligo</option>
		<option>Tipperary</option>
		<option>Tyrone</option>
		<option>Waterford</option>
		<option>Westmeath</option>
		<option>Wexford</option>
		<option>Wicklow</option>
	</select>
</p>

<h1 style="text-align:center;">Pay By Card</h1>
<p>
	<label>Cardholder Name: <br />
	<input type="text" name="donate_cardname" />
</p>

<p>
	<label>Card Number: <br />
	<input type="text" name="donate_cardnumber" />
</p>

<p>
	<label>Expiry Date: <br />
	<input type="date" name="donate_expirydate" />
</p>

<p>
	<label>Security Code: <br />
	<input type="password" name="donate_securitycode" />
</p>

<input type="Submit" value="Submit">
</div>
</form>
</body>
<footer>
<div id="footerWrap">
	<div id="Info">
		<h3>Information</h3>
		<ul>
			<a href="Hompage.html">Home</a><br/>
			<a href="AboutPage.html">About</a><br/>
			<a href="ContactPage.html">Contact</a><br/>
		</ul>
	</div>	
	<div id="AboutUs">
		<h3>About Us</h3>
		<ul>
			<a href="WhatWeDoPage.html">What We Do</a><br/>
			<a href="WhereWeWorkPage.html">Where We Work</a><br/>
			<a href="HowYouCanHelpPage.html">How You Can Help</a><br/>
		</ul>
	</div>
	<div id="WhatsHappening">
		<h3>Whats Happening</h3>
		<ul>			
			<a href="#">Events</a><br/>
			<a href="TestimonialsPage.html">Testimonials</a><br/><br/>
		</ul>
	</div>		
	<div id="DonateAndSocial">
		<br/>
		<button><a href="DonatePage.html">Donate</a></button><br/><br/><br/>  <!--Update Donate Link to page-->
		<a class="socialImg "href="https://www.facebook.com/"><img src="images\facebook.jpg" style="width:1.75em;height:1.75em;"></a>
		<a class="socialImg href="https://www.twitter.com/"><img src="images\twitter.jpg" style="width:1.75em;height:1.75em;"> </a> 
		<a class="socialImg href="https://www.instagram.com/"><img src="images\instagram.jpg" style="width:1.75em;height:1.75em;"> </a> 
		<a class="socialImg href="https://www.youtube.com/"><img src="images\youtube.png" style="width:1.75em;height:1.75em;"></a> 
	</div>			
</div>
</footer>
</html>